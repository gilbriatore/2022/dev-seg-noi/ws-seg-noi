import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Produto } from 'src/models/produto.model';
import { CadastroService } from 'src/services/cadastro.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

  produto : Produto = new Produto();

  constructor(private cadastro: CadastroService, 
       private router: Router,
       private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    const codigo = this.activatedRoute.snapshot.paramMap.get('codigo');
    this.cadastro.buscarPorCodigo(Number(codigo)).subscribe(produto => {
        this.produto = produto;
    });

  }

  atualizarProduto(){
    this.cadastro.atualizar(this.produto.codigo, this.produto).subscribe(() => {
      this.router.navigate(['/cadastros/produtos']);
    });
  }

}
